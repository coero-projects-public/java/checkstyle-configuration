# checkstyle-configuration

Add this to your java project's pom.xml to enable linting with COERO's checkstyle configuration.
```xml
<build>
    <plugins>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-checkstyle-plugin</artifactId>
            <version>3.1.2</version>
            <configuration>
                <configLocation>https://gitlab.com/coero-projects-public/java/checkstyle-configuration/-/raw/main/checkstyle.xml</configLocation>
            </configuration>
            <dependencies>
                <dependency>
                    <groupId>com.puppycrawl.tools</groupId>
                    <artifactId>checkstyle</artifactId>
                    <version>9.0</version>
                </dependency>
           </dependencies>
        </plugin>
    </plugins>
</build>
```